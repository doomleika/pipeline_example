import Phaser from 'phaser';

const config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 200 }
        }
    },
    scene: {
        preload: preload,
        create: create
    }
};
const game = new Phaser.Game(config);

import assets from 'assets/*.*';
console.log(assets);

function preload (this: Phaser.Scene)
{
    this.load.image('sky', assets.sky.png);
    this.load.image('logo', assets.logo.png);
    this.load.image('red', assets.red.png);
}
function create (this: any)
{
    this.add.image(400, 300, 'sky');
    var particles = this.add.particles('red');
    var emitter = particles.createEmitter({
        speed: 100,
        scale: { start: 1, end: 0 },
        blendMode: 'ADD'
    });
    var logo = this.physics.add.image(400, 100, 'logo');
    logo.setVelocity(100, 200);
    logo.setBounce(1, 1);
    logo.setCollideWorldBounds(true);
    emitter.startFollow(logo);
}
